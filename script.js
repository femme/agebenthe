window.onload = function() {
  // Month Day, Year Hour:Minute:Second, id-of-element-container
  countUpFromTime("Jan 28, 2019 01:34:00", 'countup'); 
};
function countUpFromTime(countFrom, id) {
  countFrom = new Date(countFrom).getTime();
  var now = new Date(),
      countFrom = new Date(countFrom),
      distance = (now - countFrom);
    
  var secondsInADay = 60 * 60 * 1000 * 24,
      secondsInAHour = 60 * 60 * 1000;
    
  days = Math.floor(distance / (secondsInADay) * 1);
  daysInYear = 365;
  years = Math.floor(days / daysInYear);
  if (years > 1) {
    // compensating for leap year
    if (years % 4 == 1) {
      daysInYear = 366;
    }
    days = days - (years * daysInYear) 
  }
  hours = Math.floor((distance % (secondsInADay)) / (secondsInAHour) * 1);
  minutes = Math.floor(((distance % (secondsInADay)) % (secondsInAHour)) / (60 * 1000) * 1);
  seconds = Math.floor((((distance % (secondsInADay)) % (secondsInAHour)) % (60 * 1000)) / 1000 * 1);

  document.getElementById("years").innerHTML = years + " jaar";

  document.getElementById("countup").innerHTML = days + " dagen " + hours + " uren "
  + minutes + " minuten " + seconds + " seconden ";

  // birthday wish
  var expr = /Jan 28/;
  if (expr.test(now)) {
    ballon = String.fromCodePoint(0x1F388);
    kusje = String.fromCodePoint(0x1F618);
    clearTimeout(countUpFromTime.interval);
    document.getElementById("birthday").innerHTML = "Gefeliciteerd! " + kusje + ballon;
  }

  // emojis for the stages of life
  if (years > 69) {
    oma = String.fromCodePoint(0x1F475);
    clearTimeout(countUpFromTime.interval);
    document.getElementById("emoji").innerHTML = oma;

  } else if (years > 17) {
    losHaarMeisje = String.fromCodePoint(0x1F469);
    clearTimeout(countUpFromTime.interval);
    document.getElementById("emoji").innerHTML = losHaarMeisje;

  } else if (years > 11) {
    staartMeisje = String.fromCodePoint(0x1F471, 0x200D, 0x2640, 0xFE0F);
    clearTimeout(countUpFromTime.interval);
    document.getElementById("emoji").innerHTML = staartMeisje;

  } else if (years > 5) {
    staartjesMeisje = String.fromCodePoint(0x1F467);
    clearTimeout(countUpFromTime.interval);
    document.getElementById("emoji").innerHTML = staartjesMeisje;

  } else if (years > 1) {
    hetMeisje = String.fromCodePoint(0x1F9D2);
    clearTimeout(countUpFromTime.interval);
    document.getElementById("emoji").innerHTML = hetMeisje;

  } else {
    baby = String.fromCodePoint(0x1F476);
    clearTimeout(countUpFromTime.interval);
    document.getElementById("emoji").innerHTML = baby;

  }

  clearTimeout(countUpFromTime.interval);
  countUpFromTime.interval = setTimeout(function(){ countUpFromTime(countFrom, id); }, 1000);
}


